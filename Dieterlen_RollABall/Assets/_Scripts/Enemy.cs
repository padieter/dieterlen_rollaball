﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{

    GameObject player; //Reference to the player game object
    Rigidbody rb; //Referenece to my rigidbody
    public float speed; //How fast I move, set in editor
    
    // Start is called before the first frame update
    void Start()
    {
        //Set up reference
        //Search for a gameobject tagged "Player", remember to add tag to player for it to work.
        player = GameObject.FindGameObjectWithTag("Player");
        rb = GetComponent<Rigidbody>(); //Find my rigidbody and save a reference to it.
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //Follow the player
        Vector3 dirtoplayer = player.transform.position - transform.position; //What is the direction between me and the player
        dirtoplayer = dirtoplayer.normalized; //Convert from whatever length this direction is to a length of 1 

        rb.AddForce(dirtoplayer * speed); //Move in direction
    }

}