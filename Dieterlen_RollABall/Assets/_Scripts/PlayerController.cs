﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement; //Lets you use scene managing code in this script

public class PlayerController : MonoBehaviour
{

    public float speed;

    public Text countText;

    public Text winText;

    private Rigidbody rb;

    private int count;



    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();

        count = 0;

        SetCountText();

        winText.text = "";

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");


        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement * speed);
    }

    // Destroy everything that enters the trigger
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            SetCountText();
            
        }
    }

    void SetCountText ()
    {
        countText.text = "Count: " + count.ToString();
        if (count >= 8)
        {
            winText.text = "You Win!";

            Invoke("RestartLevel", 2f); //Call level restart after 2 seconds
        }
    }

    void RestartLevel()
    {
        //Reload the current level:
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void OnCollisionEnter(Collision collision)
        //If we have a physical collision with a hard object we can't pass through
    {
        if (collision.other.CompareTag("Enemy")) //If the thing we ran into is tagged "Enemy"
        {
            //Requires "RestartLevel()" function written in other snippet
            RestartLevel();
        }
    }
}